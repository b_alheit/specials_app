from bs4 import BeautifulSoup
import requests
import numpy as np

restaurant_names = np.array([])
dining_out_urls = np.array([])
restaurant_urls = np.array([])
fb_urls = np.array([])

D_O_BASE_URL = "https://www.dining-out.co.za/restaurants/Cape-Town/42"

try:
    for page_num in range(141):
        if page_num == 0:
            search_page = requests.get(D_O_BASE_URL)
        else:
            search_page = requests.get(D_O_BASE_URL + "-0-" + str(page_num))

        html_obj = BeautifulSoup(search_page.text, 'html.parser')
        do_restaurant_link = html_obj.find_all('a', class_='memberName')

        for restaurant in do_restaurant_link:
            print(restaurant['href'])
            dining_out_urls = np.append(dining_out_urls, restaurant['href'])
            restaurant_names = np.append(restaurant_names, restaurant.get_text())

            restaurant_page = requests.get(restaurant['href'])
            restaurant_html_obj = BeautifulSoup(restaurant_page.text, 'html.parser')

            html_restaurant_site_container = str(restaurant_html_obj.find('div', class_='memberwebsite'))
            site_start_position = html_restaurant_site_container[html_restaurant_site_container.find("http"):]
            if site_start_position == "e":
                restaurant_urls = np.append(restaurant_urls, "")
            else:
                restaurant_urls = np.append(restaurant_urls, html_restaurant_site_container[html_restaurant_site_container.find("http"):-len("\',\'_blank\');\">Website</div>")])

            fb_button = str(html_obj.find('div', class_='MemFBPG'))
            fb_start_position = fb_button[html_restaurant_site_container.find("http"):]
            if fb_start_position == "e":
                fb_urls = np.append(fb_urls, "")
            else:
                fb_urls = np.append(fb_urls, fb_button[fb_button.find("http"):-len("\',\'_blank\');\"></div>")])

            # Save at each iteration in case of error
            np.save("restaurant_names", restaurant_names)
            np.save("do_urls", dining_out_urls)
            np.save("restaurant_urls", restaurant_urls)
            np.save("fb_urls", fb_urls)

except:
    print("There was an error.")
