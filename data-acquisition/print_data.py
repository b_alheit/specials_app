import numpy as np

DATA_DIR = "./data"
data_file_names = ["restaurant_names.npy",  # File of restaurant name
                   "restaurant_urls.npy",   # File of urls to restaurant websites
                   "do_urls.npy",           # File of urls to dining out page of specific restaurant
                   "fb_urls.npy"]           # File of urls to facebook pages of specific restaurants

for data_file in data_file_names:
    loaded_data = np.load(DATA_DIR + data_file)
    print(data_file)
    print(loaded_data)
