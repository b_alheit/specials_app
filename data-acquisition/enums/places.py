from enum import Enum


class Cities(Enum):
    CAPE_TOWN = "cape town"
    PORT_ELIZABETH = "pe"

    def get_suburbs_of_city(self):

        if type(self) != Cities:
            raise Exception("Input must be of form Cities.CITY")
            # return 10
        if self == self.CAPE_TOWN:
            return CapeTownSuburbs
        if self == self.PORT_ELIZABETH:
            return PESuburbs

        raise Exception("Input not contained in enum")



class CapeTownSuburbs(Enum):
    CONSTANTIA = "constantia"

class PESuburbs(Enum):
    CONSTANTIA = "some random thing"


print(Cities.get_suburbs_of_city(Cities.CAPE_TOWN))

# for city in Cities:
#     print(city.value)