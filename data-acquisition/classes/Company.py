import numpy as np
import pandas as pd


class Company:
    def __init__(self, name, fb_url=None, instagram_url=None):
        self.name = name
        self.fb_url = fb_url
        self.instagram_url = instagram_url


class Restaurant(Company):
    def __init__(self, name):
        Company.__init__(self, name)
        self.locations = LocationsTable()
        self.specials = None
        self.fb_posts = None


class LocationsTable(pd.DataFrame):
    def __init__(self):
        pd.DataFrame.__init__(self, columns=["city", "suburb", "longitude", "latitude"])

